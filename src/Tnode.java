import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Double.parseDouble;
import static java.lang.String.format;

/**
 * This class represents a node.
 * It is used to construct a tree from Reverse Polish Notation string
 * and return its Left Parenthetic Expression.
 */
public class Tnode {
  private String name;
  private Tnode firstChild;
  private Tnode nextSibling;

  /**
   * Constructor.
   *
   * @param name        Left Parenthetic Expression of node.
   * @param firstChild  first child of node
   * @param nextSibling next sibling of node
   */
  private Tnode(String name, Tnode firstChild, Tnode nextSibling) {
    setName(name);
    setFirstChild(firstChild);
    setNextSibling(nextSibling);
  }

  /**
   * Sets Left Parenthetic Expression.
   *
   * @param name value to be set.
   */
  private void setName(String name) {
    this.name = name;
  }

  /**
   * Sets first child of node.
   *
   * @param firstChild value to be set.
   */
  private void setFirstChild(Tnode firstChild) {
    this.firstChild = firstChild;
  }

  /**
   * Sets next rightward sibling of node.
   *
   * @param nextSibling value to be set.
   */
  private void setNextSibling(Tnode nextSibling) {
    this.nextSibling = nextSibling;
  }

  /**
   * Returns Left Parenthetic Expression of node.
   */
  @Override
  public String toString() {
    return name;
  }

  /**
   * Builds a tree containing Tnodes from Reversed Polish Notation.
   * <p>
   * Solution is built on stack.
   * The stack is used to keep track of nodes, attach siblings and children
   * and construct new nodes based on mathematical operators.
   * <p>
   * The idea to use stack came from a YouTube channel "Jenny's lectures CS/IT NET&JRF"
   * The specific video is "3.13 Expression Tree from postfix | Data structures"
   * and can be found here: https://www.youtube.com/watch?v=WHs-wSo33MM
   * <p>
   * The idea came from the video, but the code is my own.
   *
   * @param pol Reverse Polish Notation string.
   * @return node that contains entire tree.
   * @throws IllegalArgumentException if operation could not be done.
   */
  public static Tnode buildFromRPN(String pol) {
    try {
      checkInput(pol);
      String[] splitString = pol.trim().split(" ");
      Stack<Tnode> nodeStack = new Stack<>();

      for (String s : splitString) {
        if (isNumeric(s)) {
          addNewNumericNodeToStack(nodeStack, s);
        } else {
          createNewNodeFromTwoTopmostNodesAndAddItToStack(nodeStack, s);
        }
      }

      return nodeStack.pop();
    }
    catch (Exception e) {
      throw new IllegalArgumentException(format("%s. Input was: <%s>", e.getMessage(), pol));
    }
  }

  /**
   * Checks if input is valid. Throws detailed exceptions if not.
   *
   * @param pol String input to be checked.
   * @throws IllegalArgumentException if input was not valid.
   */
  private static void checkInput(String pol) {
    if (pol == null || pol.trim().isEmpty()) {
      throw new IllegalArgumentException("Input cannot be null, empty or whitespace!");
    }

    String[] splitString = pol.split(" ");

    for (String argument : splitString) {
      boolean isValidOperator = isValidMathOperator(argument);
      boolean isNumeric = isNumeric(argument);

      if (!isValidOperator && !isNumeric) {
        throw new IllegalArgumentException(format("Input contains either a non-numeric or invalid math argument <%s>", argument));
      }
    }

    if (splitString.length == 1 && !isNumeric(splitString[0])) {
      throw new IllegalArgumentException("Input cannot be a single non-numeric argument!");
    }

    int numberCount = getNumberCount(splitString);
    int mathOperatorCount = getMathOperatorCount(splitString);

    if (numberCount - mathOperatorCount > 1) {
      throw new IllegalArgumentException("There are too many numbers in input!");
    }

    if (numberCount - mathOperatorCount < 1) {
      throw new IllegalArgumentException("There are too few numbers in input!");
    }

    if (splitString.length != 1 && isNumeric(splitString[splitString.length - 1])) {
      throw new IllegalArgumentException("Input cannot end with a number!");
    }
  }

  /**
   * Returns count of numbers in input.
   *
   * @param splitString String[] to be analyzed.
   * @return int count of numbers in input.
   */
  private static int getNumberCount(String[] splitString) {
    int numberCount = 0;
    for (String s : splitString) {
      if (isNumeric(s)) {
        numberCount += 1;
      }
    }

    return numberCount;
  }

  /**
   * Returns count of math operators in input.
   *
   * @param splitString String[] to be analyzed.
   * @return int of math operator count.
   */
  private static int getMathOperatorCount(String[] splitString) {
    int mathOperatorCount = 0;
    for (String s : splitString) {
      if (!isNumeric(s)) {
        mathOperatorCount += 1;
      }
    }

    return mathOperatorCount;
  }

  /**
   * Creates new numeric node, adds node second from the top as next sibling
   * and adds new node to stack.
   *
   * @param nodeStack current stack with nodes.
   * @param s         numeric input as string.
   */
  private static void createNewNodeFromTwoTopmostNodesAndAddItToStack(Stack<Tnode> nodeStack, String s) {
    Tnode topNode = nodeStack.pop();
    Tnode secondFromTopNode = nodeStack.pop();

    String newName = createLPE(s, secondFromTopNode.name, topNode.name);
    Tnode newNode = new Tnode(newName, secondFromTopNode, null);

    nodeStack.push(newNode);
  }

  /**
   * Creates Left Parenthetic Expression (LPE)
   * from mathematical operand and two topmost nodes in stack.
   *
   * @param s         mathematical operand as String.
   * @param leftName  name of node that will be the left operand.
   * @param rightName name of node that will be the right operand.
   * @return String of LPE.
   */
  private static String createLPE(String s, String leftName, String rightName) {
    return new StringBuilder()
      .append(s)
      .append("(")
      .append(leftName)
      .append(",")
      .append(rightName)
      .append(")")
      .toString();
  }

  /**
   * Creates a new numeric node, sets it as sibling to topmost node in stack if possible
   * and puts new node on top of stack.
   *
   * @param nodeStack current stack with nodes.
   * @param s         numeric input as string.
   */
  private static void addNewNumericNodeToStack(Stack<Tnode> nodeStack, String s) {
    Tnode newNumberNode = new Tnode(s, null, null);

    if (!nodeStack.empty()) {
      Tnode topElement = nodeStack.pop();
      topElement.setNextSibling(newNumberNode);
      nodeStack.push(topElement);
    }

    nodeStack.push(newNumberNode);
  }

  /**
   * Checks if argument is numeric.
   *
   * @param argument input as String.
   * @return boolean true if is numeric, false if not.
   */
  private static boolean isNumeric(String argument) {
    try {
      parseDouble(argument);
      return true;
    }
    catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Checks if argument is a single valid math operator.
   *
   * @param operator input as String.
   * @return boolean true if is single valid math operator, false if not.
   */
  private static boolean isValidMathOperator(String operator) {
    String validMathOperators = "[+-/*]";
    Pattern pattern = Pattern.compile(validMathOperators);
    Matcher matcher = pattern.matcher(operator);

    int matches = 0;

    while (matcher.find()) {
      matches++;
    }

    return matches == 1;
  }
}
